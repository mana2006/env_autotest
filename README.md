1. Please make sure you installed `docker & docker-compose` into your computer. Run docker compose with cmd `docker-compose up -d`
2. Initial enviroment codeception `docker-compose run --rm codecept bootstrap`

3. Using recorder If you want record evidence with WebDriver (optional) in codeception.yml :

```
extensions:
    enabled:
        - Codeception\Extension\Recorder:
            delete_successful: false
```
and change to `WebDriver` if you dont want to use `PhpBrowser`

```
modules:
    enabled:
        - WebDriver:
            url: http://35.76.12.102
            browser: chrome
            host: selenium
```

4. Create first file and anything you want to test
```
docker-compose run --rm codecept generate:cest cceptance FirstExample
```

5. Run cmd for automation acceptance test

`docker-compose run --rm codecept run --steps`
